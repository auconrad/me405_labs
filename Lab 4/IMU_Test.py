from pyb import I2C


i2c = I2C(3)                         # create on bus 3
i2c = I2C(3, I2C.MASTER)             # create and init as a master
i2c.init(I2C.MASTER)
I2C.scan(i2c)