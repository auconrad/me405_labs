## @file IMU_Code.py
#
# @Author: Austin Conrad
# @Date: 5/20/2020
# @ME 405 Spring 2020
#

import utime
from pyb import I2C
from Motor_Code import MotorDriver
from Encoder_Code import EncoderDriver
#import csv #This is needed to write to a csv file for the response curve

class IMU():
    
    def __init__ (self, I2C_Obj,DEV_ADDR, OPR_MODE):
        
        self.Enable = 1         #Enables or Disables IMU Function from Main
        self.i2c = I2C_Obj
        self.DEV_ADDR = DEV_ADDR
        self.OPR_MODE = OPR_MODE
        
        self.EUL_Pitch_LSB = 0x1E    #Pitch, Rotation about x axis
        self.EUL_Roll_LSB = 0x1C     #Roll, Rotation about y axis
        self.EUL_Heading_LSB = 0x1A  #Yaw, Rotation about z axis
        
        self.GYR_DATA_X_LSB = 0X14   #Pitch, acceleration data
        self.GYR_DATA_Y_LSB = 0X16   #Roll, acceleration data
        self.GYR_DATA_Z_LSB = 0X18   #YAW, acceleration data
        
    def IMU_Enable(self):
        self.Enable = 1
        
    def IMU_Disable(self):
        self.Enable = 0
        
    def Set_Mode(self):
        if self.Enable == 1:
            self.i2c.mem_write(0xC,self.DEV_ADDR,self.OPR_MODE)
        else:
            print('IMU Not Enabled')
            
    def Calib_Chk(self):
        if self.Enable == 1:
            CALIB_STAT = 0x35
            self.Cal_Data = self.i2c.mem_read(1,self.DEV_ADDR, CALIB_STAT, timeout = 5000, addr_size = 8)          
            # Cal_Data = 11 11 11 11
            self.Mag_Cal = self.Cal_Data[0] & 0b11
            self.Acc_Cal = (self.Cal_Data[0] >> 2) & 0b11
            self.Gyr_Cal = (self.Cal_Data[0] >> 4) & 0b11
            self.Sys_Cal = (self.Cal_Data[0] >> 6) & 0b11
            print('Sys_cal = ',self.Sys_Cal)
            print('Gyr_Cal = ',self.Gyr_Cal)
            print('Acc_cal = ',self.Acc_Cal)
            print('Mag_cal = ',self.Mag_Cal)
        else:
            print('IMU Not Enabled')
            
    def Get_Euler_Ang(self):
        if self.Enable == 1:
            self.Head_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Heading_LSB, timeout = 5000, addr_size = 8)
            self.Head_Value = self.Head_Data[1] << 8 | self.Head_Data[0]
            if (self.Head_Value > 32767):
                self.Head_Value -= 65536
                self.Head_Value = float(self.Head_Value)
            self.Heading_Degree = (self.Head_Value/16)
        
            self.Roll_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Roll_LSB, timeout = 5000, addr_size = 8)
            self.Roll_Value = self.Roll_Data[1] << 8 | self.Roll_Data[0]
            if (self.Roll_Value > 32767):
                self.Roll_Value -= 65536
                self.Roll_Value = float(self.Roll_Value)
            self.Roll_Degree = (self.Roll_Value/16)
        
            self.Pitch_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Pitch_LSB, timeout = 5000, addr_size = 8)
            self.Pitch_Value = self.Pitch_Data[1] << 8 | self.Pitch_Data[0]
            if (self.Pitch_Value > 32767):
                self.Pitch_Value -= 65536
                self.Pitch_Value = float(self.Pitch_Value)
            self.Pitch_Degree = (self.Pitch_Value/16)    
            self.Euler_Ang = [self.Pitch_Degree, self.Roll_Degree, self.Heading_Degree]
            return self.Euler_Ang
        else:
            print('IMU Not Enabled')
            
    def Get_Ang_Vel(self):
        if self.Enable == 1:
            self.Head_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_Z_LSB, timeout = 5000, addr_size = 8)
            self.Head_Vel_Value = self.Head_Vel_Data[1] << 8 | self.Head_Vel_Data[0]
            if ( self.Head_Vel_Value > 32767):
                 self.Head_Vel_Value -= 65536
                 self.Head_Vel_Value = float( self.Head_Vel_Value)
            self.Heading_Vel = (self.Head_Vel_Value/16)
            
            self.Roll_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_Y_LSB, timeout = 5000, addr_size = 8)
            self.Roll_Vel_Value = self.Roll_Vel_Data[1] << 8 | self.Roll_Vel_Data[0]
            if (self.Roll_Vel_Value > 32767):
                self.Roll_Vel_Value -= 65536
                self.Roll_Vel_Value = float(self.Roll_Vel_Value)
            self.Roll_Vel = (self.Roll_Vel_Value/16)
            
            self.Pitch_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_X_LSB, timeout = 5000, addr_size = 8)
            self.Pitch_Vel_Value = self.Pitch_Vel_Data[1] << 8 | self.Pitch_Vel_Data[0]
            if (self.Pitch_Vel_Value > 32767):
                self.Pitch_Vel_Value -= 65536
                self.Pitch_Vel_Value = float(self.Pitch_Vel_Value)
            self.Pitch_Vel = (self.Pitch_Vel_Value/16)
            self.Ang_Vel = [self.Pitch_Vel,self.Roll_Vel,self.Heading_Vel]
            return self.Ang_Vel
        else:
            print('IMU Not Enabled')
            
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
