## @file Proportion_Control.py
#
# @Author: Austin Conrad
# @Date: 4/24/2020
# @ME 405 Spring 2020
#

import utime
from Motor_Code import MotorDriver
from Encoder_Code import EncoderDriver
#import csv #This is needed to write to a csv file for the response curve

class Motor_P_C():
    
    def __init__ (self, Mot, Enc):
        
        ## initializing the timer values
        # 
        #
        self.Times = ['time']
        self.Positions = ['positions']
        self.Kp = .1
        self.Err = 0
        self.time = 0
        self.Ref_Pos = 0
        #self.Position
        self.Duty =10
        self.Motor = Mot
        self.Encoder = Enc
        
    def Motor_Control(self):
        
        ## Run the motor update function
        self.Encoder.update()
        #Measure time since last tested
        #self.time = utime.ticks_ms()
        # Store the position value in a CSV
        self.Position = self.Encoder.get_position()
        self.time = utime.ticks_ms()
        self.Positions.append(self.Position)
        self.Times.append(self.time)
        #
        #with open('Response_Curve.csv', mode='w') as Response_Curve:
        #    Response_Curve = csv.writer(Response_Curve, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        #    Response_Curve.writerow([self.time, self.Position])
        #get the delta and store in error
        ## array.append
        # make an array of tupples
        # becomes an array of objects
        self.Err = self.Encoder.get_delta()
        # convert error to percent of 65535
        self.Err = self.Ref_Pos - self.Position
        self.TolBand = abs(self.Err)
        #self.Err = self.Err/65535
        # Set duty equal to the error times Pw
        self.Duty = int(self.Err*self.Kp)
        # Set saturation limit at 100%/-100% or min +-10%
        if self.Duty >= 100:
            self.Duty = 100
        elif self.Duty > 0 and self.Duty < 10:
            self.Duty = 10
        elif self.Duty < 0 and self.Duty > -10:
            self.Duty = -10
        elif self.Duty <= -100:
            self.Duty = -100
        #print('self.Duty = ',self.Duty)
        #self.Motor.set_duty(self.Duty)
    def Motor_Out(self):
        # set duty function to set the output of the motor accordingly
        self.Motor.set_duty(self.Duty)
        
    def Motor_SetPos(self,Set_Pos):
        #setting the motor position accordingly
        self.Ref_Pos = Set_Pos
    
    def Print_ResCur(self):
        #prints the array of position vs time values
        N = len(self.Times)
        print('Below is the array for the Position vs Time Curve')
        for n in range(N):
            print('{:},{:}'.format(utime.ticks_diff(self.Times[n],self.Times[0]),self.Positions[n]))
        
    def Set_Kp(self,Kp):
        self.Kp = Kp
    
    def MovToPos(self):
        #Computes the first duty prior to entering the control loop
        self.Motor_Control()
        #Loops through the control loop
        while self.TolBand > 2:
            self.Motor_Out()      #Move motor until dutycycle is found
            self.Motor_Control()  #generating duty cycle based on error
            utime.sleep_ms(5)     #delay 10 ms then repeat if self.Duty not equal to 0
        
        #Prints the time and Position array after MovToPos has been finished
        self.Duty = 0
        self.Motor_Out()
        self.Print_ResCur()
        
