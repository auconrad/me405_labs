#@Lab1_Code.py
#
#Author: Austin Conrad
#Date: 4/24/2020
#ME 405 Spring 2020
#
import pyb

class MotorDriver:
## This class builds the tools a Servo Motor and Duty Cycle
    
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        ## Creates a motor driver by initializing GPIO
        # pins and turning the motor off for safety.
        # @param EN_pin A pyb.Pin object to use as the enable pin.
        # @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
        # @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
        # @param timer A pyb.Timer object to use for PWM generation on IN1_pin
        # and IN2_pin. 
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        self.timC1 = self.timer.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        self.timC2 = self.timer.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)
        
        print ('Creating a motor driver')

    ## Enables the motor driver by setting the enable pin High
    def enable (self):
        print ('Enabling Motor')
        self.EN_pin.high()
    
    ## Disables the motor driver by setting the enable pin low
    def disable (self):
        print ('Disabling Motor')
        self.EN_pin.low()
    
    ## Sets the magnitude of the duty cycle for the motor according to the 
    #  value of duty. The direction is determined by the sign of Duty +CW,-CCW
    def set_duty (self, duty):
        
        if duty >=0: #motor moves CW direction
            self.IN2_pin.low()
            self.timC1.pulse_width_percent(duty)
        
        elif duty <= 0: #motor moves CCW direction
            self.IN1_pin.low()
            self.timC2.pulse_width_percent(abs(duty))

        
if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq = 20000);
    
    # Create a motor object passing in the pins and timer
    motor = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer)
    
    # Enable the motor driver
    motor.enable()
    
    # Set the duty cycle to 10 percent
    motor.set_duty(10)

