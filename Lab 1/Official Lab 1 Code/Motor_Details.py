## @file Motor_Details.py
#
# @page MotDet Motor Details
# 
# For the first implimentation of the motor code I didn't have doxygen running
# properly as I do now, so hopefully the commenting is improved and allows for
# an easy of use as well as a better grading experience. The main features that 
# have changed/updated since the first submition are the lines and lines of 
# documentation. Most of the information regarding it's use and code to operate
# has not actually changed since the first submission. 