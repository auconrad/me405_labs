## @file IMU_Code.py
#
# @Author: Austin Conrad
# @Date: 5/20/2020
# @ME 405 Spring 2020
#

import utime
from pyb import I2C
from Motor_Code import MotorDriver
from Encoder_Code import EncoderDriver
#import csv #This is needed to write to a csv file for the response curve

class IMU():
## This class builds the objects for communication with an IMU through I2C 
# communication proticol. The board provides positional feedback to the Nucleo 
# in the form of Euler Angles, or the angles off of zero about the Pitch, Roll,
# and Yaw or X, Y, and Z axis. These values can then be used to provide a 
# feedback loop to a given motion system. 
    
    ## Init Method: Used during specific object definition. It takes in the I2C
    # object, device address and the opperating mode of the specific IMU
    # @param I2C_Obj I2C object for the given IMU
    # @param Dev_ADDR is the address of the I2C IMU in hex
    # @param OPR_MODE is the opperating mode of the IMU
    
    def __init__ (self, I2C_Obj,DEV_ADDR, OPR_MODE):
        
        self.Enable = 1         #Enables or Disables IMU Function from Main
        self.i2c = I2C_Obj
        self.DEV_ADDR = DEV_ADDR
        self.OPR_MODE = OPR_MODE
        
        self.EUL_Pitch_LSB = 0x1E    #Pitch, Rotation about x axis
        self.EUL_Roll_LSB = 0x1C     #Roll, Rotation about y axis
        self.EUL_Heading_LSB = 0x1A  #Yaw, Rotation about z axis
        
        self.GYR_DATA_X_LSB = 0X14   #Pitch, acceleration data
        self.GYR_DATA_Y_LSB = 0X16   #Roll, acceleration data
        self.GYR_DATA_Z_LSB = 0X18   #YAW, acceleration data
    
    ## IMU Enable Method: Sets the Enable variable to 1   
    def IMU_Enable(self):
        self.Enable = 1
    ## IMU Disable Method: Sets the Enable variable to 0    
    def IMU_Disable(self):
        self.Enable = 0
    
    ## Set Mode Method: Writes the initial Operating Mode to the IMU  
    def Set_Mode(self):
        if self.Enable == 1:
            self.i2c.mem_write(0xC,self.DEV_ADDR,self.OPR_MODE)
        else:
            print('IMU Not Enabled')
    
    ## Calibration Check Method: Checks to see if the IMU is calibrated and prints the results        
    def Calib_Chk(self):
        if self.Enable == 1:
            CALIB_STAT = 0x35
            self.Cal_Data = self.i2c.mem_read(1,self.DEV_ADDR, CALIB_STAT, timeout = 5000, addr_size = 8)          
            # Cal_Data = 11 11 11 11
            self.Mag_Cal = self.Cal_Data[0] & 0b11
            self.Acc_Cal = (self.Cal_Data[0] >> 2) & 0b11
            self.Gyr_Cal = (self.Cal_Data[0] >> 4) & 0b11
            self.Sys_Cal = (self.Cal_Data[0] >> 6) & 0b11
            print('Sys_cal = ',self.Sys_Cal)
            print('Gyr_Cal = ',self.Gyr_Cal)
            print('Acc_cal = ',self.Acc_Cal)
            print('Mag_cal = ',self.Mag_Cal)
        else:
            print('IMU Not Enabled')
    
    ## Get Euler Angle Method: Retrieves the Euler Angles for Pitch, Roll and
    # Yaw, then returns these values in a tupple 
    def Get_Euler_Ang(self):
        if self.Enable == 1:
            self.Head_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Heading_LSB, timeout = 5000, addr_size = 8)
            self.Head_Value = self.Head_Data[1] << 8 | self.Head_Data[0]
            if (self.Head_Value > 32767):
                self.Head_Value -= 65536
                self.Head_Value = float(self.Head_Value)
            self.Heading_Degree = (self.Head_Value/16)
        
            self.Roll_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Roll_LSB, timeout = 5000, addr_size = 8)
            self.Roll_Value = self.Roll_Data[1] << 8 | self.Roll_Data[0]
            if (self.Roll_Value > 32767):
                self.Roll_Value -= 65536
                self.Roll_Value = float(self.Roll_Value)
            self.Roll_Degree = (self.Roll_Value/16)
        
            self.Pitch_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.EUL_Pitch_LSB, timeout = 5000, addr_size = 8)
            self.Pitch_Value = self.Pitch_Data[1] << 8 | self.Pitch_Data[0]
            if (self.Pitch_Value > 32767):
                self.Pitch_Value -= 65536
                self.Pitch_Value = float(self.Pitch_Value)
            self.Pitch_Degree = (self.Pitch_Value/16)    
            self.Euler_Ang = [self.Pitch_Degree, self.Roll_Degree, self.Heading_Degree]
            return self.Euler_Ang
        else:
            print('IMU Not Enabled')
    
    ## Get Angle Velocity Method: Gets the Angular Velocity for the Pitch, Roll and Yaw
    # axis and returns them in a tuple
    def Get_Ang_Vel(self):
        if self.Enable == 1:
            self.Head_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_Z_LSB, timeout = 5000, addr_size = 8)
            self.Head_Vel_Value = self.Head_Vel_Data[1] << 8 | self.Head_Vel_Data[0]
            if ( self.Head_Vel_Value > 32767):
                 self.Head_Vel_Value -= 65536
                 self.Head_Vel_Value = float( self.Head_Vel_Value)
            self.Heading_Vel = (self.Head_Vel_Value/16)
            
            self.Roll_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_Y_LSB, timeout = 5000, addr_size = 8)
            self.Roll_Vel_Value = self.Roll_Vel_Data[1] << 8 | self.Roll_Vel_Data[0]
            if (self.Roll_Vel_Value > 32767):
                self.Roll_Vel_Value -= 65536
                self.Roll_Vel_Value = float(self.Roll_Vel_Value)
            self.Roll_Vel = (self.Roll_Vel_Value/16)
            
            self.Pitch_Vel_Data = self.i2c.mem_read(2,self.DEV_ADDR, self.GYR_DATA_X_LSB, timeout = 5000, addr_size = 8)
            self.Pitch_Vel_Value = self.Pitch_Vel_Data[1] << 8 | self.Pitch_Vel_Data[0]
            if (self.Pitch_Vel_Value > 32767):
                self.Pitch_Vel_Value -= 65536
                self.Pitch_Vel_Value = float(self.Pitch_Vel_Value)
            self.Pitch_Vel = (self.Pitch_Vel_Value/16)
            self.Ang_Vel = [self.Pitch_Vel,self.Roll_Vel,self.Heading_Vel]
            return self.Ang_Vel
        else:
            print('IMU Not Enabled')
            
        
        
## Sample main code: 
if __name__ == '__main__':
    # IMU - Setup Variables
    i2c = I2C(1, I2C.MASTER) #Creates and inits board as master
    i2c.init(I2C.MASTER)     #Inits as master
    #DEV_ADDR = 0x28
    DEV_ADDR = 0x28
    OPR_MODE = 0x3D
    
    IMU1 = IMU(i2c,DEV_ADDR,OPR_MODE)
    IMU1.IMU_Enable()
    IMU1.Set_Mode()
    IMU1.Calib_Chk()    

    while True:
        #IMU1.Calib_Chk()
        EuAgl = IMU1.Get_Euler_Ang()
        AgVel = IMU1.Get_Ang_Vel()
        print('Euler Angle = ',EuAgl)
        print('Angular Vel = ',AgVel)
        pyb.delay(1000)
            
        
        
        
        
        
        
        
        
        
        
        
