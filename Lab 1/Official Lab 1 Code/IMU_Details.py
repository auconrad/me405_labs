## @file IMU_Details.py
#
# @page IMUDet IMU Code Details
# 
# The IMU class began as code running in main to ensure proper I2C communication.
# Once the code consistently output Euler Angle and Angle Velocity data, the 
# task became implementing these functions within an IMU class. The class will
# enable/disable the IMU through a simple enable variable checked for at the 
# start of any function within the class. 
#
# The useful functions of the IMU class include:
#
#       -Get_Euler_Angl():This function gets the Euler angles of Pitch, Roll and Yaw and returns them in a tuple 
#       -Calib_Chk(self): This function checks the calibration for Gyroscopic, Accelerometer, Magnetometer, and System. Then it returns these values in a tuple
#       -Get_Ang_Vel(self): This function gets the angular velocities using Gyr data and returns them in a tuple
# 
# Images and Video are in Lab 4 Folder:https://bitbucket.org/auconrad/me405_labs/src/master/Lab%204/
# 
#  @image html IMU_Test.JPG
#
#

