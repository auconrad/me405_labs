## @file Proportion_Control.py
#
# @Author: Austin Conrad
# @Date: 4/24/2020
# @ME 405 Spring 2020
#

import utime
from Motor_Code import MotorDriver
from Encoder_Code import EncoderDriver
#import csv #This is needed to write to a csv file for the response curve

class Motor_P_C():
## This class builds the objects for controlling a Servo Motor through the use
# of a PWM signal and the Encoder at the back. This proportional controler class
# can take in the input values of a motor and encoder object, then using methods
# such as set_pos() or set_Kp(), the motor can be controlled. One thing that 
# came out from working with Charlie on this particular controller is that it 
# isn't cooperative. This was because the move to position method actually takes
# over as it has a while loop to ensure the motor reaches the set position. 
    
    ## Init Method: Used during specific object definition. It takes in the 
    # Motor and Encoder objects to ensure proper function for the controller.
    # @param Mot A specific motor object from the motor driver class (ex: Motor1, Motor2, ect)
    # @param Enc A specific encoder object from the Encoder driver class (ex: Enc1, Enc2, ect)
    
    def __init__ (self, Mot, Enc):
        
        ## initializing the timer values
        # 
        #
        self.Times = ['time']
        self.Positions = ['positions']
        self.Kp = .1
        self.Err = 0
        self.time = 0
        self.Ref_Pos = 0
        #self.Position
        self.Duty =10
        self.DutyOld = 0
        self.Motor = Mot
        self.Encoder = Enc
    
    ## Motor Control Method: Behaves like the update comand for the motor control   
    def Motor_Control(self):
        
        ## Run the motor update function
        self.Encoder.update()
        # Store the position value in an array
        self.Position = self.Encoder.get_position()
        self.time = utime.ticks_ms()
        self.Positions.append(self.Position)
        self.Times.append(self.time)
        self.Err = self.Encoder.get_delta()
        # convert error to percent of 65535
        self.Err = self.Ref_Pos - self.Position
        self.TolBand = abs(self.Err)
        #self.Err = self.Err/65535
        # Set duty equal to the error times Pw
        self.Duty = int(self.Err*self.Kp)
        #Integral Control 
        self.Duty = self.Duty + self.DutyOld
        self.DutyOld = self.Duty

    ## Motor Out Method: Outputs a motor and the recalcualted duty cycle
    def Motor_Out(self):
        # set duty function to set the output of the motor accordingly
        self.Motor.set_duty(self.Duty)
    
    ## Motor Set Position Method: Updates the set position for the motor to move to
    # @param Set_Position Integer value of the desired encoder position for the
    # motor to move to.
    def Motor_SetPos(self,Set_Pos):
        #setting the motor position accordingly
        self.Ref_Pos = Set_Pos
    
    ## Print Response Curve Method: Prints the noted positions and time steps of response curve to shell
    def Print_ResCur(self):
        #prints the array of position vs time values
        N = len(self.Times)
        print('Below is the array for the Position vs Time Curve')
        for n in range(N):
            print('{:},{:}'.format(utime.ticks_diff(self.Times[n],self.Times[0]),self.Positions[n]))
    
    ## Set Kp Method: Set's the proportional constant Kp to the input value
    # @param Kp Proportional control constant, Kp can be a decimal value
    def Set_Kp(self,Kp):
        self.Kp = Kp
        
    ## Move To Position Method: Starts the control loop which utilizes the other
    # methods to move the motor to the set position. Not a cooperative code as
    # the loop takes over when called and kicks out once the position is reached
    def MovToPos(self):
        #Computes the first duty prior to entering the control loop
        self.Motor_Control()
        #Loops through the control loop
        while self.TolBand > 2:
            self.Motor_Out()      #Move motor until dutycycle is found
            self.Motor_Control()  #generating duty cycle based on error
            utime.sleep_ms(5)    #delay 10 ms then repeat if self.Duty not equal to 0
        #Prints the time and Position array after MovToPos has been finished
        self.Duty = 0
        self.Motor_Out()
        self.Print_ResCur()
        
if __name__ == '__main__':
    # Below exsists some example code that can be used to see how Proportional control works
    
    # Create the pin objects used for interfacing with the motor driver
    pin_EN = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
            
    # Create the timer object used for PWM generation
    timer = pyb.Timer(3, freq = 20000);
            
    # Create a motor object passing in the pins and timer
    Motor1 = MotorDriver(pin_EN, pin_IN1, pin_IN2, timer)
            
    # Enable the motor driver
    Motor1.enable()
            
    # Set the duty cycle to 10 percent
    #motor.set_duty(-10)
    
    #Setting up encoder pins for the first motor
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    #creating timer object for first encoder
    tim4 = pyb.Timer(4, prescaler=0, period = 65535)
    #
    #t4ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pinB6)
    #t4ch2 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pinB7)
    Set_Pos1 = 1000
    Pos_Old1 = 0
    Delta1 = 0
    Duty1 = 0
    Pos1 = 0    
    
    Enc1 = EncoderDriver(pinB6,pinB7,tim4)
    
    Mot1_Cntlr = Motor_P_C(Motor1, Enc1 )
    Mot1_Cntlr.Set_Kp(5)
    Mot1_Cntlr.Motor_SetPos(1000)
    #Motor1.set_duty(9)
    #utime.sleep_ms(1000)
    print('Start position is = ',Enc1.get_position())
    
    Mot1_Cntlr.MovToPos()