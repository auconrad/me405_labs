## @file Proportional_Control_Details.py
#
# @page PCDet Proportional Control Details
# 
# When building the Proportional Controller class I wanted to ensure that the
# motor did not have to land at a specific encoder tick exactly. This was for
# several reasons. The first reason is because of fact that PWM values below 
# 10 are unstable and can result in no/unpredictable motion, there is a double
# saturation block in place, and any value below ten is saturated to 10. This 
# means that at the minimum the motor will try and move into position at a speed
# that could exceed the expected. The second reason is that the provided gear
# reduction means a given encoder tick is not that relivant to the main shaft
# output.
#
# To test the motor code for this lab I began by using a fixed tolerance band 
# (the threshold at which I decided close to the set point was close engouh) and 
# varied Kp. These were not resulting in very different plots given the wide 
# range of values for Kp (.1, .75, 1.5, 5). 
#
# What it would appear this means is that for this low inertia system the motor
# is not greatly affected by the Kp value for a response time, rather it is 
# more important to have a loose enough tolerance band that the motor stops
# before getting into a vibration point. 
#
# The other possibility too is that the Kp value is well below the tested values 
# however, simple testing showed that Kp values of ~.01 worked but were very slow
#
# Images and Video are in Lab 3 Folder: https://bitbucket.org/auconrad/me405_labs/src/master/Lab%203/ 
# 
#  @image html All_Testing.JPG
#  @image html Response1.JPG
#
#

