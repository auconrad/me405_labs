## @file Encoder_Details.py
#
# @page EncDet Encoder Details
# 
# For the first implimentation of the code, I simple made sure the encoder was 
# able to update the major variables needed to run the motor such as position
# and delta, being sure to monitor overflow/underflow issues in regards to delta.
# One issue that arrose was the total position of the encoder. A missunderstanding
# between Charlie and myself resulted in the total position variable being 
# incorrectly implemented. A few more meetings and the new total position variable
# should be correctly implemented by useing a total position variable which 
# is simply updated by taking total position and adding delta to it. There aren't
# any overflow issues now due to the fact that the delta function already has 
# over/under flow implimented. 
#

