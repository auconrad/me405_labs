var classProportion__Control_1_1Motor__P__C =
[
    [ "__init__", "classProportion__Control_1_1Motor__P__C.html#a7026f33e20e9576a0b7d782e7a6bbb80", null ],
    [ "Motor_Control", "classProportion__Control_1_1Motor__P__C.html#aee39b8b920804af17d656a41756a1f8f", null ],
    [ "Motor_Out", "classProportion__Control_1_1Motor__P__C.html#ad72a5d77a140637bb35be1798b1e9c7e", null ],
    [ "Motor_SetPos", "classProportion__Control_1_1Motor__P__C.html#a87a06df4cb8bb5482d8377ea1e192339", null ],
    [ "MovToPos", "classProportion__Control_1_1Motor__P__C.html#a06c00d8f5e4f885b520b76f057b129da", null ],
    [ "Print_ResCur", "classProportion__Control_1_1Motor__P__C.html#a392bc62b432e9162bfae6e22f75e58a7", null ],
    [ "Set_Kp", "classProportion__Control_1_1Motor__P__C.html#ac237528fe23822d54d4d825de08bd9ff", null ],
    [ "Duty", "classProportion__Control_1_1Motor__P__C.html#afea4f08714f59701325367d85d2aa574", null ],
    [ "DutyOld", "classProportion__Control_1_1Motor__P__C.html#ab8a8dd0d60a8af6e0a8bd5f5d8891797", null ],
    [ "Encoder", "classProportion__Control_1_1Motor__P__C.html#a4d3e21c66e5b5e9c02283899e8e44117", null ],
    [ "Err", "classProportion__Control_1_1Motor__P__C.html#a1232c86de11afc20426f6ef62185770d", null ],
    [ "Kp", "classProportion__Control_1_1Motor__P__C.html#a1997e7390540074f97c7770c478a43dd", null ],
    [ "Motor", "classProportion__Control_1_1Motor__P__C.html#aa68c1f0ae4ddbbfe51748925ae148270", null ],
    [ "Position", "classProportion__Control_1_1Motor__P__C.html#a1366b25766c267eb1a12d8f154a3bb8f", null ],
    [ "Positions", "classProportion__Control_1_1Motor__P__C.html#a28baa671748e61891186889766dd9562", null ],
    [ "Ref_Pos", "classProportion__Control_1_1Motor__P__C.html#a7b49ab751751240dbf56a70b78c5ba65", null ],
    [ "time", "classProportion__Control_1_1Motor__P__C.html#a553d8ebad1d9de7e9aeb70be432db05a", null ],
    [ "Times", "classProportion__Control_1_1Motor__P__C.html#a1800bbc9853e457a6e4868f649aaff3f", null ],
    [ "TolBand", "classProportion__Control_1_1Motor__P__C.html#af53ab369d8c11218ddd05b64c34471fe", null ]
];