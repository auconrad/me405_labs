# @file Motor_Code.py
#
#Author: Austin Conrad
#Date: 5/30/2020
#ME 405 Spring 2020
#

import pyb

class MotorDriver:
## This class builds the objects for controlling a Servo Motor through the use
# of a PWM signal. This class can enable/disable the motor as well as set the 
# duty cycle according to user input with positive inputs being clockwise and 
# negative inputs being counter clockwise. To see an example of the code running,
# see the Motor_Code.py file located in the link below and run the code located
# below the if __name__ == '__main__': section of the same file.Link: https://bitbucket.org/auconrad/me405_labs/src/master/Lab%201/Official%20Lab%201%20Code/
    
    ## Creates a motor driver by initializing GPIO
    # pins and turning the motor off for safety.
    # @param EN_pin A pyb.Pin object to use as the enable pin.
    # @param IN1_pin A pyb.Pin object to use as the input to half bridge 1.
    # @param IN2_pin A pyb.Pin object to use as the input to half bridge 2.
    # @param timer A pyb.Timer object to use for PWM generation on IN1_pin
    # and IN2_pin. 
    def __init__ (self, EN_pin, IN1_pin, IN2_pin, timer):
        
        self.EN_pin = EN_pin
        self.IN1_pin = IN1_pin
        self.IN2_pin = IN2_pin
        self.timer = timer
        
        self.timC1 = self.timer.channel(1, pyb.Timer.PWM, pin = IN1_pin)
        self.timC2 = self.timer.channel(2, pyb.Timer.PWM, pin = IN2_pin)
        # self.timC1 = self.timer.channel(1, pyb.Timer.PWM, pin = self.IN1_pin)
        # self.timC2 = self.timer.channel(2, pyb.Timer.PWM, pin = self.IN2_pin)
        #print ('Creating a motor driver')

    ## Enables the motor driver by setting the enable pin High (no input required)
    def enable (self):
        #print ('Enabling Motor')
        self.EN_pin.high()
    
    ## Disables the motor driver by setting the enable pin low (no input required)
    def disable (self):
        #print ('Disabling Motor')
        self.EN_pin.low()
    
    ## Sets the magnitude of the duty cycle for the motor according to the 
    # value of duty. The direction is determined by the sign of Duty +CW,-CCW.
    # The function used to generate the PWM signal has saturation limits built 
    # in meaning a signal greater than 100 can be passed in without harming the
    # motor (though a signal greater than 100 has no effect on the output speed).
    # @param duty Input duty cycle, can be positive or negative and it will be 
    # saturated to a 100% duty cycle
    def set_duty (self, duty):
        
        self.duty = duty
        if duty >= 0: #motor moves CW direction
            self.timC2.pulse_width_percent(0)
            self.timC1.pulse_width_percent(self.duty)
        
        elif duty <= 0: #motor moves CCW direction
            self.timC1.pulse_width_percent(0)
            self.timC2.pulse_width_percent(abs(self.duty))

## Sample main code: 
if __name__ == '__main__':
    ## Motor Setup Variables
    # Create the pin objects used for Motor 1 (Pitch)interfacing with the motor driver
    pin_EN1 = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
    pin_IN1_1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
    pin_IN2_1 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);
    
    #Pitch Timer object for used for PWM generation
    timer1 = pyb.Timer(3, freq = 20000);
          
    #Pitch motor object passing in the pins and timer
    Motor1 = MotorDriver(pin_EN1, pin_IN1_1, pin_IN2_1, timer1)
    Motor1.enable()
        
    # Set the duty cycle to 10 percent
    Motor1.set_duty(50) 

