## @file main.py
#  This is the main page for the code used all labs
#
#  
#  Detailed doc for main.py 
#
#  @author Austin Conrad
#
#  @copyright License Info
#
#  @date 5/22/2020
#
#

##---- Required Files ------ 
# These are the files other than main that must be on the board to function properly:
# Encoder_Code.py
# Motor_Code.py
# IMU_Code.py
# 


import utime
from Motor_Code import MotorDriver
from Encoder_Code import EncoderDriver
from pyb import I2C
from IMU_Code import IMU

# IMU - Setup Variables
i2c = I2C(1, I2C.MASTER) #Creates and inits board as master
i2c.init(I2C.MASTER)     #Inits as master
#DEV_ADDR = 0x28
DEV_ADDR = 0x28
OPR_MODE = 0x3D

#IMU Object initialization
IMU1 = IMU(i2c,DEV_ADDR,OPR_MODE)
IMU1.IMU_Enable()
IMU1.Set_Mode()
IMU1.Calib_Chk()

#---- Pitch Axis Setup Variables and Class Objects -----

# Create the pin objects used for Motor 1 (Pitch)interfacing with the motor driver
pin_EN1 = pyb.Pin(pyb.Pin.cpu.A10, pyb.Pin.OUT_PP);
pin_IN1_1 = pyb.Pin(pyb.Pin.cpu.B4, pyb.Pin.OUT_PP);
pin_IN2_1 = pyb.Pin(pyb.Pin.cpu.B5, pyb.Pin.OUT_PP);

#Pitch Timer object for used for PWM generation
timer1 = pyb.Timer(3, freq = 20000);
      
#Pitch motor object passing in the pins and timer
Motor1 = MotorDriver(pin_EN1, pin_IN1_1, pin_IN2_1, timer1)
Motor1.enable()

#creating timer object for Pitch encoder
tim4 = pyb.Timer(4, prescaler=0, period = 65535)

#Pitch Encoder Pins
pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
# Encoder Timer setup
t4ch1_1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pinB6)
t4ch2_1 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pinB7)  
# Encoder Definition
Enc1 = EncoderDriver(pinB6,pinB7,tim4)

#---- Roll Axis Setup Variables and Class Objects ------

# Create the pin objects used for Motor 2 (Roll) interfacing with the motor driver
pin_EN2 = pyb.Pin(pyb.Pin.cpu.C1, pyb.Pin.OUT_PP);
pin_IN1_2 = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.OUT_PP);
pin_IN2_2 = pyb.Pin(pyb.Pin.cpu.A1, pyb.Pin.OUT_PP);

#Roll Timer object for used for PWM generation
timer2 = pyb.Timer(5, freq = 20000);

#Roll motor object passing in the pins and timer
Motor2 = MotorDriver(pin_EN2, pin_IN1_2, pin_IN2_2, timer2)
Motor2.enable()

#creating timer object for Roll encoder
tim8 = pyb.Timer(8, prescaler=0, period = 65535)

#Roll Encoder Pins
pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
pinC7 = pyb.Pin(pyb.Pin.cpu.C7)

#Roll Encoder Timer setup
t8ch1_2 = tim8.channel(1, pyb.Timer.ENC_AB, pin = pinC6)
t8ch2_2 = tim8.channel(2, pyb.Timer.ENC_AB, pin = pinC7)  

#Roll Encoder Definition
Enc2 = EncoderDriver(pinC6,pinC7,tim8)


#------- Controler Variables: -------------------
#Error = 0
Error_Sum_Pitch = 0
Duty_Pitch = 0
Error_Sum_Roll = 0
Duty_Roll = 0
Vmax = 800
MaxAng = 45
Kp = .04
Ki = .0001
Time_New = 0


while True: 
    #----IMU Data Collection (Get Angle) ----
    EuAgl = IMU1.Get_Euler_Ang()
    Pitch = EuAgl[0]
    Roll = -EuAgl[1]
    #----IMU Angle to Velocity Req ----------
    Vset_Pitch = (-Pitch/MaxAng)*Vmax
    Vset_Roll = (-Roll/MaxAng)*Vmax
    #----Encoder Velocity Calculator---------
    Enc1.update()
    Enc2.update()
    Time_Old = Time_New
    Time_New = utime.ticks_ms() #dt calculation:
    dt = Time_New - Time_Old
    Vact_Pitch = (Enc1.get_delta())/dt
    Vact_Roll = (Enc2.get_delta())/dt
    #----Error Calculation ------------------
    Error_Pitch = Vset_Pitch - Vact_Pitch
    Error_Sum_Pitch = Error_Sum_Pitch + Error_Pitch
    Error_Roll = Vset_Roll - Vact_Roll
    Error_Sum_Roll = Error_Sum_Roll + Error_Roll
    #----Duty Calculation ------------------
    Duty_Pitch = Error_Pitch*Kp + Error_Sum_Pitch*Ki
    Duty_Roll = Error_Roll*Kp + Error_Sum_Roll*Ki
    #----Motor1 Out-------------
    # Pitch Motor Limit Controled Output
    if (-45 < Pitch) and (Pitch < 45):
        Motor1.set_duty(Duty_Pitch)
    else:
        Motor1.set_duty(0)
    
    if (-45 < Roll) and (Roll < 45):
        Motor2.set_duty(Duty_Roll)
    else:
        Motor2.set_duty(0)
    
    pyb.delay(10)
