## @file mainpage.py
#  This is the documentation of the ME 405 lab files.
#
#  Within this webpage one can find information on the the project  
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This project will build some cool stuff to annoy younger syblings with.
#  Though the goal was to have self supported folders for each lab, they don't 
#  cooperate with doxygen well and are mostly used as a back up folder as well
#  as a location for photos and videos of outputs for each lab if the doxygen 
#  file doesn't go through. Thus this link is the best to see the folder with 
#  the up to date code files:
#  https://bitbucket.org/auconrad/me405_labs/src/master/Lab%201/Official%20Lab%201%20Code/
#
#  @section sec_mot Motor Driver
#  Some information about the motor driver with links. Please see motor.Motor which is part of the \ref motor package.
#  https://bitbucket.org/auconrad/me405_labs/src/master/Lab%201/Official%20Lab%201%20Code/ 
#
#  @section sec_enc Encoder Driver
#  Some information about the encoder driver with links. Please see encoder.Encoder which is part of the \ref encoder package.
#  https://bitbucket.org/auconrad/me405_labs/src/master/Lab%201/Official%20Lab%201%20Code/ 
#
#  @section sec_Pcont Proportional Controller
#  The proportional controller class can be used to drive a motor to a set point. See the Code for more information
#  
#  @author Austin Conrad
#
#  @copyright License Info
#
#  @date April 30, 2020
#