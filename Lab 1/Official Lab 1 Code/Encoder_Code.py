## @file Encoder_Code.py
#
# @Author: Austin Conrad
# @Date: 5/30/2020
# @ME 405 Spring 2020
#

import pyb


class EncoderDriver():
## This class can be used to generate specific encoder objects to be used for 
# controling/monitoring the position of a servo motor. The required inputs are
# two encoder pin objects and one timer object. The encoder class will keep
# track of the global position of the encoder as well as the incremental
# changes (delta) between update calls. This delta can be used in other programs
# along with a timer call to generate velocities. To see the code working, run
# the Encoder_Code.py code from the link below and use the code found below
# the if __name__ == '__main__': section to see it working
    
    
    ## Init Method: Used during specific object definition. It takes in the 
    # two encoder pins and timer object to ensure proper function for the encoder.
    # @param A_pin A pyb.Pin object used for interpreting encoder signal
    # @param B_pin A pyb.Pin object used for interpreting encoder signal
    # @param TImer A Timer object used for the proper reading and writing to 
    # the encoder being used.
    # Link: https://bitbucket.org/auconrad/me405_labs/src/master/Lab%201/Official%20Lab%201%20Code/
    
    def __init__ (self,A_pin, B_pin, Timer):
        
        #timer and pin specific variables for instance of EncoderDriver
        self.A_pin = A_pin
        self.B_pin = B_pin
        self.Timer = Timer
        self.Timer_Chan1 = Timer.channel(1, pyb.Timer.ENC_AB, pin = self.A_pin)    
        self.Timer_Chan2 = Timer.channel(2, pyb.Timer.ENC_AB, pin = self.B_pin)
        #Initializing the Pos_Tot value and pos_cur value
        self.Pos_Tot = 0
        self.pos_cur = self.Timer.counter()
        print ('Creating an Encoder Object')

    ## Update Method: Reads current encoder value and updates Pos_Tot, and Delta
    def update (self): 
        #Read the encoder value and call it the current position
        self.pos_old = self.pos_cur
        self.pos_cur = self.Timer.counter()
        self.delta = self.pos_cur - self.pos_old
        #Overflow/Underflow logic
        if self.delta < -32767:
            self.delta = 65535 - self.pos_old + self.pos_cur
        elif self.delta >= 32767:
            self.delta = 65535 + self.pos_old - self.pos_cur
        else:
            return
        #Returning the corrected delta value
        self.Pos_Tot = self.Pos_Tot + self.delta
        #print("Current Position " + str(self.pos_cur) +" ticks")
    
    ## Get Position Method: Returns the total position of the encoder from the 
    # intialized zero    
    def get_position (self):
        #Return the current position
        #print("Get Position Fucntion")
        return self.Pos_Tot
    
    ## Set Position Method: Forces the total position value of the encoder to 
    # the input argument value
    # @param set_pos Integer input argument to set the total encoder position 
    def set_position (self,set_pos):
        self.Pos_Tot = int(set_pos)
        self.Timer.counter(set_pos)
        self.pos_old = self.pos_cur
        self.pos_cur = self.Timer.counter()
    
    ## Get Delta Method: Returns the calculated Delta value obtained after the 
    # Update method has been run    
    def get_delta (self):
        return self.delta 


if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver   
    
    #Pitch Encoder Pins
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    #creating timer object for Pitch encoder
    tim4 = pyb.Timer(4, prescaler=0, period = 65535)
    # Encoder Timer setup
    t4ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pinB6)
    t4ch2 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pinB7)  
    # Encoder Definition
    Enc1 = EncoderDriver(pinB6,pinB7,tim4)
    #Pitch Encoder Pins

    while True:
        Enc1.update()
        Pos = Enc1.get_position()
        print('Current Position is: ',Pos,' ticks.')
        pyb.delay(100)










