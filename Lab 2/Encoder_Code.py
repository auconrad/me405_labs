## @file Encoder_Code.py
#
# @Author: Austin Conrad
# @Date: 4/24/2020
# @ME 405 Spring 2020
#

import pyb

class EncoderDriver():
    
    def __init__ (self,A_pin, B_pin, Timer):
        ## initializing the timer values
        #
        #
        #timer and pin specific variables for instance of EncoderDriver
        self.A_pin = A_pin
        self.B_pin = B_pin
        self.Timer = Timer
        #self.Prescaler = Prescaler
        #self.Timer = pyb.Timer(self.TimNum)
        self.Timer_Chan1 = Timer.channel(1, pyb.Timer.ENC_AB, pin = self.A_pin)    
        self.Timer_Chan2 = Timer.channel(2, pyb.Timer.ENC_AB, pin = self.B_pin)
        
        self.pos_cur = self.Timer.counter()
        print ('Creating an Encoder Object')

    def update (self):
        #Read the encoder value and call it the current position
        #self.timer.counter()
        #print("Update Fucntion")
        self.pos_old = self.pos_cur
        self.pos_cur = self.Timer.counter()
        self.delta = self.pos_cur - self.pos_old
        #Overflow/Underflow logic
        if self.delta < -32767:
            self.delta = 65535 - self.pos_old + self.pos_cur
        elif self.delta >= 32767:
            self.delta = 65535 + self.pos_old - self.pos_cur
        #Returning the corrected delta value
        #print("Current Position " + str(self.pos_cur) +" ticks")
        
    def get_position (self):
        #Return the current position
        #print("Get Position Fucntion")
        return self.pos_cur
    
    def set_position (self,set_pos):
        #Set the current value to the value passed in
        #print("Set Position Fucntion")
        self.pos_cur = set_pos
        self.Timer.counter(set_pos)
        self.pos_old = self.pos_cur
        self.pos_cur = self.Timer.counter()
        
    def get_delta (self):
        #Compared with the last encoder capture what is delta
        #print("Get Delta Fucntion")
        return self.delta 
        
        
    
#timer initializing:


## if main section here for testing
if __name__ == '__main__':
    # Create the pin objects used for interfacing with the motor driver   
    
    #Setting up encoder pins for the first motor
    pinB6 = pyb.Pin(pyb.Pin.cpu.B6)
    pinB7 = pyb.Pin(pyb.Pin.cpu.B7)
    #creating timer object for first encoder
    tim4 = pyb.Timer(4, prescaler=0, period = 65535)
    #
    #t4ch1 = tim4.channel(1, pyb.Timer.ENC_AB, pin = pinB6)
    #t4ch2 = tim4.channel(2, pyb.Timer.ENC_AB, pin = pinB7)
    Pos_Cur4 = 0
    Pos_Old4 = 0
    Delta4 = 0
    
    Enc1 = EncoderDriver(pinB6,pinB7,tim4)
    
    #Setting up encoder pins for the first motor
    pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
    pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
    #creating timer object for first encoder
    tim6 = pyb.Timer(6, prescaler=0, period = 65535)
   
    Pos_Cur6 = 0
    Pos_Old6 = 0
    Delta6 = 0
 
    Enc2 = EncoderDriver(pinC6, pinC7, tim5)
    
    Enc1.update()
    Pos_Cur4 = Enc1.get_position()
    print("Current Position is: " + str(Pos_Cur4) + " ticks.")
    Enc1.set_position(20)
    Pos_Cur4 = Enc1.get_position()
    print("Current Position is: " + str(Pos_Cur4) + " ticks.")
    #print("New Set position is " + str(Pos_Cur4) + "ticks.")
    #Enc1.get_delta()
    
    
    # ## second channel stuff for now
    # tim8 = pyb.Timer(8, prescaler = 0, period = 65535)
    # pinC6 = pyb.Pin(pyb.Pin.cpu.C6)
    # pinC7 = pyb.Pin(pyb.Pin.cpu.C7)
    # t8ch1 = tim8.channel(1, pyb.Timer.ENC_AB, pin = pinC6)
    # #t8ch2 = tim8.channel(1, pyb.Timer.ENC_B, pin = pinC7)









