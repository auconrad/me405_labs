''' @file Lab_0_Fib.py'''
"""
Created on Fri Apr 10 15:00:14 2020
@author: Austin
"""
## fin(Input) is the Fibonacci Number Calculator base on the user defined
#  index value n, entered as the input argument. The calculator works by
#  counting from an index value of 0 up to the user's defined index. The number
#  is then returned into the variable Fib_Num to be displayed for the user

def fib (Input):
    
    Output = 0
    Last = 0
    Temp = 1
    Index = 0
    #Loop counts up to the Fib# until index = 0
    while (Input != 0):
        Input = Input - 1
        Index = Index + 1
        Last = Temp
        Temp = Output
        Output = Output + Last
    return Output #output is the Fib # at the specified Index
        
''' This method calculates a Fibonacci number corresponding to
a specified index.'''
'''@param Input An integer specifying the index of the desired
Fibonacci number.'''

if __name__ == '__main__':
    State = True
    while State: 
        Fib_Num = 0 #Setting the initial Fib_Num = 0
        user_in = input('Input the index of a desired Fibinacci Number (ie: 0,1,2 ...): ')
        int(user_in) #taking in the user input and converting it to an int
        print('Calculating Fibonacci number at index n = ')
        print(user_in)#returning the user value to show the input was recieved
        Fib_Num = fib(int(user_in))
        print('The calculated Fib number is: ')
        print(Fib_Num) #Displaying the Fib #
        Play = input('Would you like to calculate another number? (y)es or (n): ')
        if Play == 'y' or Play == 'Y': #Checking to see if the user wants to play again
            State = True
        elif Play == 'n' or Play == 'N': #If no thanks for playing
            State = False
            print('Thanks for using the Fibonacci Calculator!')
        else: #else invalid response, exit
            State = False
            print('Invalid Response')        

    









