## @file Term_Proposal.py
#
# @page TermProp Term Project Proposal
# 
# For the term project, Zack Gordon and myself were going to try and build
# a 2 DOF leveling bot. The form factor would be similar to a gimbal but with
# only 2 motors, so we wont be able to control heading. For hardware/required
# sensors we should be able to encorporate both motors, the imu and encoders 
# for the project. The major items we will have to modify and incorporate to 
# ensure we get proper function are the following:
# -Proportional_Control: add integral action and eliminate dead band % Saturation Band
#       - Remove the internal loop to ensure proper cooperative multitasking
# -Motor_Code: determine encoderticks/rev to accurately feed new setpoints from
# IMU feedback into the motor controler
# 
# Timeline/Flowchart:
# -Update all required code in prep for cooperative multitasking
# -Get single motor moving in response to IMU Euler Angles
# -Get two Motors moving in response to IMU Euler Angles
# -Build foam core/cardboard frame for assembly
# -Tune and play until functional
# 
# 

